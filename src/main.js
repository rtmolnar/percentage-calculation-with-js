'use strict';

var defaultQtd = 20;
var defaultPercentage = 100
var maxQtd = 20;

var currentQtd = defaultQtd;
var currentPercentage = defaultPercentage;

var percentageunit =  100/maxQtd


function counter() {
  let seconds = 0;
  setInterval(() => {
    seconds += 1;
    document.getElementById('app').innerHTML = `<p>You have been here for ${seconds} seconds.</p>`;
  }, 1000);
}

function subtract(){
  currentQtd -= 1;
  currentPercentage = currentPercentage += percentageunit
  setUp();
}

function addUp(){

  currentQtd += 1;
  currentPercentage = currentPercentage -= percentageunit
  setUp();
}

function setUp(){
  document.getElementById('removable')?.remove();
  document.getElementById('app').innerHTML = `
  <div id="removable">
  <p>The current quantity is ${currentQtd}. </p>
  <p>The current percentage is ${currentPercentage}. </p><br>
  </div>
  `
}

setUp()